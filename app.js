'use strict';
var path = require('path');
var extract = require('pdf-text-extract')
var pdfUtil = require('pdf-to-text');
var mysql = require('mysql');
var fs = require('fs');
var https = require('https');
var azureaccesskey = '074dfabbe89c4f9f8d69509f281bf36f';
var azureuri = 'westus.api.cognitive.microsoft.com';

// var conn = mysql.createConnection({
//     host: "localhost",
//     user: "root",
//     password: "",
//     database: "azure"
// });

// Create Database in local mysql...
// conn.connect(function(error){
//     if(error){
//         throw error;
//     }
//     conn.query("CREATE DATABASE IF NOT EXISTS azure", function(error, result){
//         if(error) { throw error; return; }
//         console.log("Azure Database Created");
//         // var sql = "CREATE TABLE azure (id INT UNSIGNED NOT NULL AUTO_INCREAMENT, ds_createdTime DATETIME NOT NULL, ds_modifiedTime DATETIME NOT NULL, ds_text TEXT NOT NULL, ds_language VARCHAR(30), ds_sentiment VARCHAR(30), ds_keyPhrase TEXT NOT NULL, ds_entities TEXT NOT NULL)";
//         // conn.query(sql, function(error, result){
//         //     if(error){
//         //         throw error;
//         //     }
//         //     console.log("Table Created");
//         // })
//     })
// })

var connect = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "azure"
});

function storeDatatoSQL(filename, createdTime, modifiedTime, text, language, sentiment, keyphrase, entities){
    connect.connect(function(error){
        var sql = "Insert INTO documents(ds_url, ds_savetime, ds_updatetime, ds_text, ds_lang, ds_sentiment, ds_keyphrase, ds_entities) VALUES ('" + filename + "','" + createdTime + "','" + modifiedTime + "','" + text + "','" + language + "','" + sentiment + "','" + keyphrase + "','" + entities + "')";
        connect.query(sql, function (err, result) {
            // up????
            if (err) throw err;
            console.log(filename+" data stored to sql!!!");
        });
    })
}

// storeDatatoSQL ("1.pdf", "createdTime", "modeifiedTime", "text", "language", "sentiment", "keyphrase", "entities");

function readfilesfromdir(dirname){
    if (!fs.existsSync(path.join(__dirname,dirname))){
        console.log(dirname + " Directory not Exist");
        process.exit(-1);
    }
    var dirpath = path.join(__dirname, dirname);

    return new Promise((resolve, result) => {
        fs.readdir(dirpath, function(error, items){
            resolve(items);
        })
    });
}

// readfilesfromdir('testdocs').then(result => {
//     console.log(result);
// });

function extracttextfrompdf(dirname, filename){
    var filepath = path.join(__dirname, dirname, filename);
    
    var realtext = '';
    return new Promise((resolve, reject) => {
        extract(filepath, { splitPages: false }, (err, text) => {
            if(err){
                console.dir(err)
                return
            }
            for (var i = 0; i < text.length; i++){
                realtext += text[i];
            }
            resolve({text: realtext, filename});
        })
    })
}
// Only Extracting text from searchable pdf....
// extracttextfrompdf('testdocs','asamblea_estatutos_corfadich.pdf').then(result => {
//     console.log(result);
// })

function get5120charsfromtext(text){
    return text.substring(0,5119);
}
// extracttextfrompdf('testdocs','asamblea_estatutos_corfadich.pdf').then(result => {
//     console.log(get5120charsfromtext(result));
// })

function getlanguagefromtext(filename, text, callback){
    let path = '/text/analytics/v2.1/languages';
    let response_handler = function (response) {
        let body = '';
        response.on ('data', function (d) {
            body += d;
        });
        response.on ('end', function () {
            let body_ = JSON.parse (body);
            let body__ = JSON.stringify (body_, null, '  ');
            callback(body__, filename)
        });
        response.on ('error', function (e) {
            console.log ('Error: ' + e.message);
        });
    };

    let get_language = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : azureuri,
            path : path,
            headers : {
                'Ocp-Apim-Subscription-Key' : azureaccesskey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }
    let documents = { 'documents': [
        { 'id': filename, 'text': text },
    ]};
    get_language(documents);
}

// extracttextfrompdf('testdocs','asamblea_estatutos_corfadich.pdf').then(result => {
//     // console.log(get5120charsfromtext(result));
//     getlanguagefromtext("1.txt", get5120charsfromtext(result), (lang) =>{
//         console.log(lang);
//     })
// });

function getsentimentfromtext(filename, text, callback){
    let path = '/text/analytics/v2.1/sentiment';
    let response_handler = function (response) {
        let body = '';
        response.on ('data', function (d) {
            body += d;
        });
        response.on ('end', function () {
            let body_ = JSON.parse (body);
            let body__ = JSON.stringify (body_, null, '  ');
            callback(body__, filename)
        });
        response.on ('error', function (e) {
            console.log ('Error: ' + e.message);
        });
    };

    let get_sentiments = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : azureuri,
            path : path,
            headers : {
                'Ocp-Apim-Subscription-Key' : azureaccesskey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }

    let documents = { 'documents': [
        { 'id': filename, 'text': text },
    ]};

    get_sentiments(documents);
}

// extracttextfrompdf('testdocs','asamblea_estatutos_corfadich.pdf').then(result => {
//     // console.log(get5120charsfromtext(result));
//     getsentimentfromtext("1.txt", get5120charsfromtext(result), (lang) =>{
//         console.log(lang);
//     })
// });


function getkeyphrasesfromtext(filename, text, callback){
    let path = '/text/analytics/v2.1/keyPhrases';
    let response_handler = function (response) {
        let body = '';
        response.on ('data', function (d) {
            body += d;
        });
        response.on ('end', function () {
            let body_ = JSON.parse (body);
            let body__ = JSON.stringify (body_, null, '  ');
            callback(body__, filename)
        });
        response.on ('error', function (e) {
            console.log ('Error: ' + e.message);
        });
    };
    let get_key_phrases = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : azureuri,
            path : path,
            headers : {
                'Ocp-Apim-Subscription-Key' : azureaccesskey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }

    let documents = { 'documents': [
        { 'id': filename, 'text': text },
    ]};
    get_key_phrases(documents);
}

// extracttextfrompdf('testdocs','asamblea_estatutos_corfadich.pdf').then(result => {
//     // console.log(get5120charsfromtext(result));
//     getkeyphrasesfromtext("1.txt", get5120charsfromtext(result), (lang) =>{
//         console.log(lang);
//     })
// });


function getentitiesfromtext(filename, text, callback){
    let path = '/text/analytics/v2.1/entities';
    let tempFileName = filename;
    let response_handler = (response) => {
        let body = '';
        response.on ('data', (d) => {
            body += d;
        });
        response.on ('end', () => {
            let body_ = JSON.parse (body);
            let body__ = JSON.stringify (body_, null, '  ');
            callback(body__, tempFileName)
        });
        response.on ('error', (e) => {
            console.log ('Error: ' + e.message);
        });
    };
    let get_entities = (documents) => {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : azureuri,
            path : path,
            headers : {
                'Ocp-Apim-Subscription-Key' : azureaccesskey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }

    let documents = { 'documents': [
        { 'id': filename, 'text': text },
    ]};
    get_entities(documents);
}

// extracttextfrompdf('testdocs','asamblea_estatutos_corfadich.pdf').then(result => {
//     // console.log(get5120charsfromtext(result));
//     getentitiesfromtext("1.txt", get5120charsfromtext(result), (lang) =>{
//         console.log(lang);
//     })
// });


function getcreatedTime(filename, dirname){
    var filePath = path.join(__dirname, dirname, filename)
    return new Promise((resolve, reject) =>{
        pdfUtil.info(filePath, function(err, info){
            if(err) throw(err)
            var vals = info['creationdate'];
            resolve({createdTime: vals, filename});
        })
    });
}

function getmodifiedTime(filename, dirname){
    var filePath = path.join(__dirname, dirname, filename)
    return new Promise((resolve, reject) =>{
        pdfUtil.info(filePath, function(err, info){
            if(err) throw(err)
            var vals = info['moddate'];
            resolve({modifiedTime: vals, filename});
        })
    });
}

function movefile(filename, fromdir, todir){
    var sourcepath = path.join(__dirname, fromdir, filename);
    var destpath = path.join(__dirname, todir);
    
    var f = path.basename(sourcepath);
    var dest = path.resolve(destpath, f);

    fs.rename(sourcepath, dest, (err) =>{
        if(err) throw err;
        else console.log(filename + " Successfully moved");
    })

}


function main(){
    return new Promise((resolve, reject) => {
        var nonprocessed = 'nonprocessed';  // dir_name of non processed pdf files
        var processed = 'processed';     // dir_name of processed pdf files

        // Get filenames from non processed directory; 
        readfilesfromdir(nonprocessed).then(filenames => {
            for (var i = 0; i < filenames.length; i++){
                if (!filenames[i]) continue;
                // console.log(filenames[i]);

                extracttextfrompdf(nonprocessed,filenames[i]).then(({text, filename}) => {
                    getlanguagefromtext(filename, get5120charsfromtext(text), (lang, filename) =>{
                        getsentimentfromtext(filename, get5120charsfromtext(text), (sentiment, filename) => {
                            getkeyphrasesfromtext(filename, get5120charsfromtext(text), (keyphrase, filename) => {
                                getentitiesfromtext(filename, get5120charsfromtext(text), (entities, filename) =>{
                                    getcreatedTime(filename, nonprocessed).then( ({createdTime, filename}) => {
                                        getmodifiedTime(filename, nonprocessed).then( ({modifiedTime, filename}) => {
                                            storeDatatoSQL(filename, createdTime, modifiedTime, text, lang, sentiment, keyphrase, entities);
                                            movefile(filenames,nonprocessed, processed);
                                        })
                                    })
                                })
                            })
                        })
                    })
                })

                
            }
        });
    })
}

main();










