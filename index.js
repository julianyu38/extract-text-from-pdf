'use strict';
var path = require('path');
var extract = require('pdf-text-extract')
var pdfUtil = require('pdf-to-text');
var mysql = require('mysql');
var conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database:"azure"
});

function datatosql(filename, createdTime, modifiedTime, text, analysisedData){
    conn.connect(function(err){
        var sql = "Insert INTO documents(ds_url, ds_savetime, ds_updatetime, ds_text, ds_analyzedData) VALUES ('" + filename + "','" + createdTime + "','" + modifiedTime + "','" + text + "','" + analysisedData + "')";
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
        });

    })
}

function extractTextFromPDF(filename){
    var filePath = path.join(__dirname, 'testdocs', filename)
    var realFullText = '';
    return new Promise((resolve, reject) => {
        extract(filePath, { splitPages: false }, function(err, text) {
            if(err){
                console.dir(err)
                return
            }
            for (var i = 0; i < text.length; i++){
                realFullText += text[i];
            }
            resolve(realFullText);
        })
    });
}

function savedDataFromPDF(filename){
    var filePath = path.join(__dirname, 'testdocs', filename)
    return new Promise((resolve, reject) =>{
        pdfUtil.info(filePath, function(err, info){
            if(err) throw(err)
            var vals = Object.keys(info).map(function(key) {
                return info[key];
            });
            resolve(vals[4]);
        })
    });
}

function modifiedDataFromPDF(filename){
    var filePath = path.join(__dirname, 'testdocs', filename)
    return new Promise((resolve, reject) =>{
        pdfUtil.info(filePath, function(err, info){
            if(err) throw(err)
            var vals = Object.keys(info).map(function(key) {
                return info[key];
            });
            resolve(vals[5]);
        })
    });
}

function analyzedData(filename, text, callback){
    let https = require ('https');
    let accessKey = '074dfabbe89c4f9f8d69509f281bf36f';
    let uri = 'westus.api.cognitive.microsoft.com';
    let pathLang = '/text/analytics/v2.1/languages';
    let pathSentiment = '/text/analytics/v2.1/sentiment';
    let pathkeyPhrase = '/text/analytics/v2.1/keyPhrases';
    let pathEntity = '/text/analytics/v2.1/entities';
    let analyazedData = '';
    let response_handler = function (response) {
        let body = '';
        response.on ('data', function (d) {
            body += d;
        });
        response.on ('end', function () {
            let body_ = JSON.parse (body);
            let body__ = JSON.stringify (body_, null, '  ');
            callback(body__)
        });
        response.on ('error', function (e) {
            console.log ('Error: ' + e.message);
        });
    };
    let get_language = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : uri,
            path : pathLang,
            headers : {
                'Ocp-Apim-Subscription-Key' : accessKey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }
    let get_sentiments = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : uri,
            path : pathSentiment,
            headers : {
                'Ocp-Apim-Subscription-Key' : accessKey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }
    let get_key_phrases = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : uri,
            path : pathkeyPhrase,
            headers : {
                'Ocp-Apim-Subscription-Key' : accessKey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }
    let get_entities = function (documents) {
        let body = JSON.stringify (documents);
        let request_params = {
            method : 'POST',
            hostname : uri,
            path : pathEntity,
            headers : {
                'Ocp-Apim-Subscription-Key' : accessKey,
            }
        };
        let req = https.request (request_params, response_handler);
        req.write (body);
        req.end ();
    }

    let documents = { 'documents': [
        { 'id': filename, 'text': text },
    ]};

    get_language(documents);
    get_sentiments (documents);
    get_key_phrases (documents);
    get_entities (documents);
}

// savedDataFromPDF('1.pdf').then(result=>{
//     // result = Created time;
// })

// modifiedDataFromPDF('1.pdf').then(result=>{
//     // result = modified time;
// })

// datatosql('1.txt','11/21/13 11:24:22 Eastern Standard Time', '11/21/13 11:24:22 Eastern Standard Time', 'sadf','sadfa');

extractTextFromPDF('1.pdf').then(result => {
    var text = result;
    var first5120text = result.substring(0,5119);

    console.log("AAAAAAAA");
    analyzedData("1.pdf",first5120text, (body) => {
        console.log("BBBBBBBBB");
        savedDataFromPDF('1.pdf').then(result=>{
            console.log("CCCCCCCCc");
            var createdTime = result;
            var date = createdTime.substring(0,8);
            var datearray = date.split("/");
            createdTime = "20"+datearray[2]+"-"+datearray[0]+"-"+datearray[1];
            modifiedDataFromPDF('1.pdf').then(result=>{
                console.log("DDDDDDDD");
                var modifiedTime = result;
                var date = modifiedTime.substring(0,8);
                var datearray = date.split("/");
                modifiedTime = "20"+datearray[2]+"-"+datearray[0]+"-"+datearray[1];
                datatosql("1.pdf",createdTime, modifiedTime, text, body);
            })        
        })      
    });
});

